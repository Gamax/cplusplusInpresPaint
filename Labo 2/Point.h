
#ifndef POINT_H
#define POINT_H



class Point{

private:
    int x;
    int y;
    static int compteur;

public:
    Point(void);

    Point(int x, int y);

    ~Point();

    Point(const Point& pointtocopy);

    int getX() const;

    void setX(int X);

    int getY() const;

    void setY(int Y);

    static int getCompteur();

    void Affiche() const;
};

#endif