#include <iostream>
#include <string.h>
#include "Couleur.h"

using namespace std;

int Couleur::compteur = 0;

Couleur Couleur::ROUGE(255,0,0,"Rouge");
Couleur Couleur::VERT(0,255,0,"Vert");
Couleur Couleur::BLEU(0,0,255,"Bleu");

Couleur::Couleur(void)
{
	setRouge(0);
	setVert(0);
	setBleu(0);
	nom=NULL;
	setNom("Noir");
	compteur++;
}


//constructeur
Couleur::Couleur(int r,int v,int b,const char *n)
{
	setRouge(r);
	setVert(v);
	setBleu(b);
	nom=NULL;
	setNom(n);
	compteur++;
}

Couleur::Couleur(int r, int v, int b){
	setRouge(r);
	setVert(v);
	setBleu(b);
	nom=NULL;
	compteur++;
	
}


//copie
Couleur::Couleur(const Couleur& c){

	setRouge(c.getRouge());
	setVert(c.getVert());
	setBleu(c.getBleu());
	nom=NULL;
	setNom(c.getNom());
	compteur++;
}


//destructeur
Couleur::~Couleur(void)
{
	if(nom) delete nom;
	compteur--;
}


//setters
void Couleur::setRouge(int r)
{
	if(r>=0&&r<=255)
		rouge=r;
}

void Couleur::setVert(int v)
{
	if(v>=0&&v<=255)
		vert=v;
}

void Couleur::setBleu(int b)
{
	if(b>=0&&b<=255)
		bleu=b;
}

void Couleur::setNom(const char*n)
{
	if(nom) delete nom;
	if(!n)
		nom=NULL;
	else
	{
		nom=new char[strlen(n)+1];
		strcpy(nom,n);
	}
}


//getters
int Couleur::getRouge() const{ return rouge;}

int Couleur::getVert() const{ return vert;}

int Couleur::getBleu() const{ return bleu;}

int Couleur::getCompteur(){
	return compteur;
}

const char * Couleur::getNom() const
{
    if(nom)
        return nom;
    return NULL;

}

//autre methode
void Couleur::Affiche(void) const
{


	cout<<"Rouge="<<rouge<<endl<<"Vert="<<vert<<endl<<"Bleu="<<bleu<<endl;
	
	if(nom){
		cout<<"Nom="<<nom<<endl;
	}
}
