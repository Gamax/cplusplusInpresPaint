#include "Forme.h"
#include <iostream>
#include <string.h>

using namespace std;

int Forme::compteur = 0;

Forme::Forme() {
    setId(NULL);
    setProfondeur(0);
    setCouleur(NULL);
    pos.setX(0);
    pos.setY(0);
    compteur++;
}

Forme::Forme(char *id, const Point &pos,Couleur* couleur, int profondeur) {
    setId(id);
    setPosition(pos);
    setProfondeur(profondeur);
    setCouleur(couleur);
    compteur++;
}

Forme::Forme(const Forme &forme) {
    setId(forme.getId());
    setPosition(forme.getPosition());
    setProfondeur(forme.getProfondeur());
    setCouleur(forme.getCouleur());
    compteur++;
}

Forme::~Forme() {
    if(id!=NULL)
        delete(id);
    compteur--;
}

//setter-getter

char *Forme::getId() const {
    return id;
}

Point Forme::getPosition() const {
    return pos;
}

Couleur *Forme::getCouleur() const {
    return couleur;
}

int Forme::getProfondeur() const {
    return profondeur;
}

int Forme::getCompteur(){
    return compteur;
}

void Forme::setId(char *id) {
    if(this->id != NULL)
        delete(this->id);

    if(id == NULL){
        this->id = NULL;
    }else{

        this->id = new char[strlen(id)+1];
        strcpy(this->id,id);
    }
}

void Forme::setPosition(const Point &pos) {
    Forme::pos = pos;
}

void Forme::setCouleur(Couleur *couleur) {
    Forme::couleur = couleur;
}

void Forme::setProfondeur(int profondeur) {
    Forme::profondeur = profondeur;
}

void Forme::Affiche() const {

    cout<<"Nom : ";

    if(id != NULL){
        cout << id;
    }else{
        cout << "Aucun";
    }

    cout<<endl<<"Pos : ("<<pos.getX()<<","<<pos.getY()<<")"<<endl<<"Couleur : ";

    if(couleur != NULL && couleur->getNom() != NULL){
        cout<<couleur->getNom();
    }else{
        cout<<"Aucune";
    }

    cout<<endl<<"Profondeur : "<<profondeur<<endl;


}


