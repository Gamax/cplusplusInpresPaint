#include <iostream>
#include <string.h>
#include <stdio.h>
#include "Couleur.h"
#include "InvalidColorException.h"
#include <fstream>

int Couleur::compteur = 0;

Couleur Couleur::ROUGE(255,0,0,"Rouge");
Couleur Couleur::VERT(0,255,0,"Vert");
Couleur Couleur::BLEU(0,0,255,"Bleu");

Couleur::Couleur(void)
{
	setRouge(0);
	setVert(0);
	setBleu(0);
	nom=NULL;
	setNom("Noir");
	compteur++;
}


//constructeur
Couleur::Couleur(int r,int v,int b,const char *n)
{

    bool boolr = true,boolv = true ,boolb = true;

    if(r<0 || r>255)
        boolr = false;

    if(v<0 || v>255)
        boolv = false;

    if(b<0 || b>255)
        boolb = false;

    if(!boolr || !boolv || !boolb)
        throw InvalidColorException("Création Couleur Impossible !",boolr,boolv,boolb);

    setRouge(r);
	setVert(v);
	setBleu(b);
	nom=NULL;
	setNom(n);
	compteur++;
}

Couleur::Couleur(int r, int v, int b){

    bool boolr = true,boolv = true ,boolb = true;

    if(r<0 || r>255)
        boolr = false;

    if(v<0 || v>255)
        boolv = false;

    if(b<0 || b>255)
        boolb = false;

    if(!boolr || !boolv || !boolb)
        throw InvalidColorException("Création Couleur Impossible !",boolr,boolv,boolb);

	setRouge(r);
	setVert(v);
	setBleu(b);
	nom=NULL;
	compteur++;
	
}


//copie
Couleur::Couleur(const Couleur& c){

	setRouge(c.getRouge());
	setVert(c.getVert());
	setBleu(c.getBleu());
	nom=NULL;
	setNom(c.getNom());
	compteur++;
}


//destructeur
Couleur::~Couleur()
{
	if(nom) delete nom;
	compteur--;
}

//operator
Couleur& Couleur::operator= (Couleur c){
    setRouge(c.getRouge());
    setVert(c.getVert());
    setBleu(c.getBleu());
    setNom(c.getNom());
    return *this;
}

Couleur Couleur::operator+ (Couleur c){

    int Result[3],i;

    Result[0] = rouge+c.getRouge();
    Result[1] = vert+c.getVert();
    Result[2] = bleu+c.getBleu();

    for(i=0;i<3;i++)
        if(Result[i]>255)
            Result[i]=255;

    if(nom == NULL || c.getNom() == NULL)
        return Couleur(Result[0],Result[1],Result[2],"Melange inconnu");

    char *temp = new char[18 + strlen(nom)+strlen(c.getNom())];

    sprintf(temp,"Melange de %s et de %s",nom,c.getNom());

    Couleur  tempColor(Result[0],Result[1],Result[2], temp);

    delete(temp);

    return tempColor;
}

Couleur Couleur::operator+ (int l){

    int Result[3],i;

    Result[0] = rouge+l;
    Result[1] = vert+l;
    Result[2] = bleu+l;

    for(i=0;i<3;i++)
        if(Result[i]>255)
            Result[i]=255;

    return Couleur(Result[0],Result[1],Result[2],this->getNom());

}

Couleur operator+ (int l,Couleur c){

    int Result[3],i;

    Result[0] = c.getRouge()+l;
    Result[1] = c.getVert()+l;
    Result[2] = c.getBleu()+l;

    for(i=0;i<3;i++)
        if(Result[i]>255)
            Result[i]=255;

    return Couleur(Result[0],Result[1],Result[2],c.getNom());

}

Couleur Couleur::operator- (int l){

    int Result[3],i;

    Result[0] = rouge-l;
    Result[1] = vert-l;
    Result[2] = bleu-l;

    for(i=0;i<3;i++)
        if(Result[i]<0)
            Result[i]=0;

    return Couleur(Result[0],Result[1],Result[2],this->getNom());

}

bool Couleur::operator> (Couleur c){
    return this->getLuminance() > c.getLuminance();
}

bool Couleur::operator== (Couleur c){
    return this->getLuminance() == c.getLuminance();
}

bool Couleur::operator< (Couleur c){
    return this->getLuminance() < c.getLuminance();
}

ostream& operator<<(ostream& stream, const Couleur& c){

    if(c.getNom()!=NULL) {
        stream << "[" << c.getRouge() << "," << c.getVert() << "," << c.getBleu() << ", (" << c.getNom() << ") ]";
    }else{
        stream << "[" << c.getRouge() << "," << c.getVert() << "," << c.getBleu() << ", (Aucun nom) ]";
    }

    return stream;
}

istream& operator>>(istream& stream, Couleur& c){

    int r,v,b;
    char buffer[500];

    stream >> r >> v >> b >> buffer;

    c.setRouge(r);
    c.setVert(v);
    c.setBleu(b);
    c.setNom(buffer);
    return stream;
}

Couleur Couleur::operator++(){
    *this = (*this) + 10;
    return *this;
}

Couleur Couleur::operator++(int){
    Couleur c = *this;
    *this = (*this) + 10;
    return c;
}

Couleur Couleur::operator--(){
    *this = (*this) - 10;
    return *this;
}

Couleur Couleur::operator--(int){
    Couleur c = *this;
    *this = (*this) - 10;
    return c;
}


//setters
void Couleur::setRouge(int r)
{
	if(r>=0&&r<=255){
        rouge=r;
    }else{
        throw InvalidColorException("Modification Couleur Impossible !",false,true,true);
    }

}

void Couleur::setVert(int v)
{
	if(v>=0&&v<=255){
		vert=v;
    }else{
        throw InvalidColorException("Modification Couleur Impossible !",true,false,true);
    }
}

void Couleur::setBleu(int b)
{
	if(b>=0&&b<=255){
		bleu=b;
    }else{
        throw InvalidColorException("Modification Couleur Impossible !",true,true,false);
    }
}

void Couleur::setNom(const char*n)
{
	if(nom) delete nom;
	if(!n)
		nom=NULL;
	else
	{
		nom=new char[strlen(n)+1];
		strcpy(nom,n);
	}
}


//getters
int Couleur::getRouge() const{ return rouge;}

int Couleur::getVert() const{ return vert;}

int Couleur::getBleu() const{ return bleu;}

int Couleur::getCompteur(){
	return compteur;
}

const char * Couleur::getNom() const
{
    if(nom)
        return nom;
    return NULL;

}

int Couleur::getLuminance() const{
	return (rouge + vert + bleu)/3;
}

//autre methode
void Couleur::Affiche(void) const
{
	cout<<*this<<endl;
}

void Couleur::Save(ofstream& fichier) const
{
	cout<<"Save Couleur"<<endl;
	
    unsigned short longueur;

	int red = getRouge();
	fichier.write((char*)&red, sizeof(int));
	
	int blue = getBleu();
	fichier.write((char*)&blue, sizeof(int));
	
	int green = getVert();
	fichier.write((char*)&green, sizeof(int));
	
    if(getNom() == NULL)
        longueur = 0;
    else
    longueur = strlen(getNom());
    fichier.write((char*)&longueur, sizeof(unsigned short));
    
    if(longueur != 0)
        fichier.write((char*)getNom(), longueur);
    
	
	
	
}

void Couleur::Load(ifstream& fichier)
{
	cout<<"Load Couleur"<<endl;
	
	int red = 0;
	fichier.read((char*)&red, sizeof(int));
	setRouge(red);
	
	int blue = 0;
	fichier.read((char*)&blue, sizeof(int));
	setBleu(blue);
	
	int green = 0;
	fichier.read((char*)&green, sizeof(int));
	setVert(green);

	unsigned short longueur = 0;
    fichier.read((char*)&longueur, sizeof(unsigned short));

    if(longueur != 0)
    {
         char * buffer = new char[longueur +1];
        fichier.read((char*)buffer, longueur);
        buffer[longueur] = 0;
        setNom(buffer);
        delete buffer;
    }
    else
        setNom(NULL);
    
	

}











