#ifndef CPLUSPLUSINPRESPAINT_BASEEXCEPTION_H
#define CPLUSPLUSINPRESPAINT_BASEEXCEPTION_H

#include <exception>

using namespace std;

class BaseException : public exception {

private:
    char* message;

public:
    BaseException(char* msg);

    char* getMessage() const;

    ~BaseException() throw();

};


#endif //CPLUSPLUSINPRESPAINT_BASEEXCEPTION_H
