
#include "ListeTriee.h"

template<class T>
T *ListeTriee<T>::insere(const T &val) {
    Cellule<T> *temp, *temp2;

    if(ListeBase<T>::pTete == NULL){ //cas où la liste est vide
        ListeBase<T>::pTete = new Cellule<T>;
        ListeBase<T>::pTete->valeur = val;
        ListeBase<T>::pTete->suivant = NULL;
        return &ListeBase<T>::pTete->valeur;
    }

    temp = ListeBase<T>::pTete;

    if(ListeBase<T>::pTete->valeur > val){ //cas ou la valeur doit être insérée au début de la liste
        ListeBase<T>::pTete = new Cellule<T>;
        ListeBase<T>::pTete->valeur = val;
        ListeBase<T>::pTete->suivant = temp;
        return &ListeBase<T>::pTete->valeur;
    }

    //trouver l'endroit où insérer notre val

    while(temp->suivant != NULL && temp->suivant->valeur < val){
        temp = temp->suivant;
    }


    temp2 = temp->suivant; //on mémorise la cellule suivant l'endroit d'insertion
    temp->suivant = new Cellule<T>;
    temp=temp->suivant;
    temp->valeur = val;
    temp->suivant = temp2; //on accroche la fin de la liste chaînée

    return &temp->valeur;
}
