//
// Created by thomas on 19/01/18.
//

#include "SmartPointer.h"

template<class T>
SmartPointer<T>::SmartPointer() {
    val = NULL;
}

template<class T>
SmartPointer<T>::SmartPointer(T *t) {
    val = t;
}

template<class T>
SmartPointer<T>::SmartPointer(const SmartPointer<T> &base) {
    val = base.getVal();
}

template<class T>
SmartPointer<T>::~SmartPointer() {

}

template<class T>
void SmartPointer<T>::Delete() {
    if(val)
        delete val;
    val = NULL;
}

template<class T>
SmartPointer<T> &SmartPointer<T>::operator=(const SmartPointer<T> &base) {
    val = base.getVal();

    return *this;
}

template<class T>
SmartPointer<T> &SmartPointer<T>::operator=(T *base) {
    val = base;
    return *this;
}

template<class T>
T &SmartPointer<T>::operator*() const{
    return *val;
}

template<class T>
T *SmartPointer<T>::operator->() const{
    return val;
}

template<class T>
bool SmartPointer<T>::operator<(const SmartPointer<T> &right) const {
    return *(this->val) < *(right.val);
}

template<class T>
bool SmartPointer<T>::operator>(const SmartPointer<T> &right) const {
    return *(this->val) > *(right.val);
}

template<class T>
bool SmartPointer<T>::operator==(const SmartPointer<T> &right) const {
    return *(this->val) == *(right.val);
}

template <class T>
ostream& operator<<(ostream &s, const SmartPointer<T>& t){
    s << *t << endl;
    return s;
}



template<class T>
T *SmartPointer<T>::getVal() const{
    return val;
}

template ostream& operator<< <int>(ostream &s, const SmartPointer<int>& t);
template ostream& operator<< <Forme>(ostream &s, const SmartPointer<Forme>& t);


