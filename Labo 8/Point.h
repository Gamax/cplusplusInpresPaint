
#ifndef POINT_H
#define POINT_H

#include <iostream>

using namespace std;

class Point{

    friend ostream& operator<<(ostream& stream, const Point& p);

private:
    int x;
    int y;
    static int compteur;

public:
    Point(void);

    Point(int x, int y);

    ~Point();

    Point(const Point& pointtocopy);

    int getX() const;

    void setX(int X);

    int getY() const;

    void setY(int Y);

    static int getCompteur();

    void Affiche() const;
    
    void Save(ofstream& fichier) const;
    void Load(ifstream& fichier);
};

#endif
