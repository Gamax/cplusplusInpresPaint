#include <cstdio>
#include <iostream>
#include "ListeBase.h"
using namespace std;

template<class T>
ListeBase<T>::ListeBase() {
    pTete = NULL;
}

template<class T>
ListeBase<T>::ListeBase(const ListeBase<T> &liste) {

    if (liste.pTete != NULL) { //verif qu'il y a au moins un elem dans la liste

        pTete = new Cellule<T>();
        pTete->valeur = liste.pTete->valeur;

        Cellule<T> *tempCopy = pTete, *temp = liste.pTete;

        while (temp->suivant != NULL) {
            tempCopy->suivant = new Cellule<T>();

            tempCopy = tempCopy->suivant;
            temp = temp->suivant;

            tempCopy->valeur = temp->valeur;

        }

        temp->suivant = NULL;
    } else {
        pTete = NULL;
    }
}

template<class T>
ListeBase<T>::~ListeBase() {
    Cellule<T> *temp, *tempLast;

    temp = pTete;

    while(temp != NULL){
        tempLast= temp;
        temp= temp->suivant;
        delete (tempLast);
    }
}

template<class T>
bool ListeBase<T>::estVide() const{
    if(pTete == NULL)
        return true;
    return false;
}

template<class T>
int ListeBase<T>::getNombreElements() const{

    Cellule<T> *temp;
    int nombre = 0;

    temp = pTete;

    while(temp != NULL){
        temp= temp->suivant;
        nombre++;
    }

    return nombre;
}

template<class T>
void ListeBase<T>::Affiche() const{
    cout << "----------LISTE----------" << endl;
    Cellule<T> *temp;
    int nombre = 0;

    temp = pTete;

    while(temp != NULL){
        cout << "["<< nombre << "] : " << temp->valeur << endl;
        temp= temp->suivant;
        nombre++;
    }
    cout << "-------------------------" << endl;
}

template<class T>
ListeBase<T> &ListeBase<T>::operator=(const ListeBase<T> &liste) {

    Cellule<T> *temp, *tempLast, *tempCopy;

    //nettoyage de la liste

    temp = pTete;

    while(temp != NULL){
        tempLast= temp;
        temp= temp->suivant;
        delete (tempLast);
    }

    if(liste.pTete == NULL)
        return *this;

    pTete = new Cellule<T>();
    pTete->valeur = liste.pTete->valeur;

    tempCopy = pTete;
    temp = liste.pTete;

    while (temp->suivant != NULL) {
        tempCopy->suivant = new Cellule<T>();

        tempCopy=tempCopy->suivant;
        temp=temp->suivant;

        tempCopy->valeur = temp->valeur;

    }

    temp->suivant = NULL;

    return *this;

}
