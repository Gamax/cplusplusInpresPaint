

#ifndef CPLUSPLUSINPRESPAINT_LISTEBASE_H
#define CPLUSPLUSINPRESPAINT_LISTEBASE_H

#include "Couleur.h"
#include "SmartPointer.h"

template<class T> struct Cellule {
    T valeur ;
    Cellule<T> *suivant;
};

template<class T> class Iterateur;

template<class T> class ListeBase {

    protected :
    Cellule<T> *pTete ;

    public:
    ListeBase();
    ListeBase(const ListeBase<T> &liste);
    ~ListeBase();
    bool estVide() const;
    int getNombreElements() const;
    void Affiche() const;
    virtual T* insere(const T & val) = 0;
    ListeBase<T>& operator=(const ListeBase<T> &liste);
    friend class Iterateur<T>;



};

template class ListeBase<int>;
template class ListeBase<int*>;
template class ListeBase<SmartPointer<int> >;
template class ListeBase<SmartPointer<Forme> >;
template class ListeBase<Couleur>;

#endif //CPLUSPLUSINPRESPAINT_LISTEBASE_H
