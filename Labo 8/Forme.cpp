#include "Forme.h"
#include <iostream>
#include <string.h>
#include <fstream>

using namespace std;

int Forme::compteur = 0;

Forme::Forme() {
    id=NULL;
    setProfondeur(0);
    setCouleur(NULL);
    pos.setX(0);
    pos.setY(0);
    infos = NULL;
    compteur++;
}

Forme::Forme(const Point &pos,Couleur* couleur, int profondeur) {
    setPosition(pos);
    setProfondeur(profondeur);
    setCouleur(couleur);
    infos = NULL;
    compteur++;
}

Forme::Forme(const Forme &forme) {
    setPosition(forme.getPosition());
    setProfondeur(forme.getProfondeur());
    setCouleur(forme.getCouleur());
    infos = NULL;
    compteur++;
}

Forme::~Forme() {
    if(id != NULL)
        delete(id);
    if(infos != NULL)
        delete(infos);
    compteur--;
}

//setter-getter

char *Forme::getId() const {
    return id;
}

Point Forme::getPosition() const {
    return pos;
}

Couleur *Forme::getCouleur() const {
    return couleur;
}

int Forme::getProfondeur() const {
    return profondeur;
}

int Forme::getCompteur(){
    return compteur;
}

void Forme::setPosition(const Point &pos) {
    Forme::pos = pos;
}

void Forme::setCouleur(Couleur *couleur) {
    Forme::couleur = couleur;
}

void Forme::setProfondeur(int profondeur) {
    Forme::profondeur = profondeur;
}

bool Forme::operator<(const Forme &right) const{
    return this->profondeur < right.profondeur;
}

bool Forme::operator>(const Forme &right) const{
    return this->profondeur > right.profondeur;
}

bool Forme::operator==(const Forme &right) const{
    return this->profondeur == right.profondeur;
}

ostream &operator<<(ostream &stream, const Forme &f) {
    stream << "Profondeur : " << f.profondeur << endl;
    return stream;
}

void Forme::Save(ofstream& fichier) const
{
    unsigned short longueur = strlen(getId());
    fichier.write((char*)&longueur, sizeof(unsigned short));
    fichier.write((char*)getId(), longueur);

    pos.Save(fichier);

    couleur->Save(fichier);

    int prof = getProfondeur();
    fichier.write((char*)&prof, sizeof(int));


}


void Forme::Load(ifstream& fichier)
{

    unsigned short longueur = 0;
    fichier.read((char*)&longueur, sizeof(unsigned short));
    char *buffer= new char[longueur+1];
    fichier.read((char*)buffer, longueur);
    buffer[longueur] = 0;
    setId(buffer);
    delete buffer;

    pos.Load(fichier);
    couleur = new Couleur;
    couleur->Load(fichier);

    int pro = 0;
    fichier.read((char*)&pro, sizeof(int));
    setProfondeur(pro);

}

void Forme::Affiche() const {

    cout<<"Nom : ";

    if(id != NULL){
        cout << id;
    }else{
        cout << "Aucun";
    }

    cout<<endl<<"Pos : ("<<pos.getX()<<","<<pos.getY()<<")"<<endl<<"Couleur : ";

    if(couleur != NULL && couleur->getNom() != NULL){
        cout<<couleur->getNom();
    }else{
        cout<<"Aucune";
    }

    cout<<endl<<"Profondeur : "<<profondeur<<endl;

}

int Forme::nChiffre(int i) //utilisé pr calculer le nombre de chiffres dans un nombre (je suppose que les coordonnées
// et la profondeur ne dépasse pas les 10 chiffres)
{
    if (i < 0) i = -i;
    if (i <         10) return 1;
    if (i <        100) return 2;
    if (i <       1000) return 3;
    if (i <      10000) return 4;
    if (i <     100000) return 5;
    if (i <    1000000) return 6;
    if (i <   10000000) return 7;
    if (i <  100000000) return 8;
    if (i < 1000000000) return 9;
    return 10;
}


