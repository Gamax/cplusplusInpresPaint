#ifndef CPLUSPLUSINPRESPAINT_INVALIDCOLOREXCEPTION_H
#define CPLUSPLUSINPRESPAINT_INVALIDCOLOREXCEPTION_H

#include "BaseException.h"

class InvalidColorException : public BaseException{

private:
    bool r,v,b;

public:
    InvalidColorException(char* msg,bool r,bool v,bool b);
    bool rougeValide() const;
    bool vertValide() const;
    bool bleuValide() const;

};


#endif //CPLUSPLUSINPRESPAINT_INVALIDCOLOREXCEPTION_H
