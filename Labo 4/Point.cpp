
#include <iostream>
#include"Point.h"


using namespace std;

int Point::compteur = 0;

Point::Point() {
    x = 0;
    y = 0;
    compteur++;
}

Point::Point(int x, int y) {
    this->x = x;
    this->y = y;
    compteur++;
}

Point::~Point() {
    compteur--;
}

Point::Point(const Point &pointtocopy) {
    setX(pointtocopy.getX());
    setY(pointtocopy.getY());
    compteur++;
}

int Point::getX() const {
    return x;
}

void Point::setX(int X) {
    x = X;
}

int Point::getY() const {
    return y;
}

void Point::setY(int Y) {
    y = Y;
}

int Point::getCompteur() {
    return compteur;
}

ostream& operator<<(ostream& stream, const Point& p){
    stream<<"("<<p.getX()<<","<<p.getY()<<")";

    return stream;
}

void Point::Affiche() const {
    cout<<this<<endl;
}


