#include <string.h>
#include <stdio.h>
#include "Ligne.h"

Ligne::Ligne(void) : Forme(){
    extremite.setX(0);
    extremite.setY(0);
}

Ligne::Ligne(char *id,const Point &pos,const Point &ext,Couleur* couleur, int profondeur)
        : Forme(id,pos,couleur,profondeur){
    setExtremite(ext);
}

Ligne::Ligne(const Ligne& ligne) : Forme(ligne){
    setExtremite(ligne.getExtremite());
}

Point Ligne::getExtremite() const{
    return extremite;
}

void Ligne::setExtremite(const Point &p){
    Ligne::extremite = p;
}

ostream& operator<<(ostream& stream, const Ligne& l){

    stream << "[";

    if(l.getId()!=NULL){
        stream << l.getId();
    }else{
        stream << "Aucun nom";
    }

    stream << " : Position(" << l.getPosition().getX() << "," << l.getPosition().getY() << "),Extremite("
           << l.getExtremite().getX() <<","<< l.getExtremite().getY()<<"),";

    if(l.getCouleur()!= NULL){ //pas de couleur
        stream << "Couleur(" << l.getCouleur()->getRouge() << "," << l.getCouleur()->getVert() << ","
               << l.getCouleur()->getBleu()<<")";

    }else{
        stream << "Aucune couleur";
    }

    stream << ",Profondeur=" << l.getProfondeur()<<"]";

    return stream;
}

char* Ligne::getInfos() {

    if(infos != NULL)
        delete(infos);

    //génération de la partie couleur de la string

    char *tempCouleur;

    if(getCouleur() !=NULL){

        tempCouleur = new char[22];

        sprintf(tempCouleur,"Couleur(%d,%d,%d)",getCouleur()->getRouge(),getCouleur()->getVert(),getCouleur()->getBleu());

    }else{ //pas de couleur

        tempCouleur = new char[15];

        strcpy(tempCouleur,"Aucune Couleur");
    }

    char* tempId;

    if(getId() != NULL){
        tempId = new char[strlen(getId())];

        strcpy(tempId,getId());
    }else{ //pas de nom de forme
        tempId = new char[10];

        strcpy(tempId,"Aucun nom");
    }

    infos = new char[strlen(tempId)+nChiffre(getPosition().getX()) +nChiffre(getPosition().getY())
                     +nChiffre(getExtremite().getX())+nChiffre(getExtremite().getY())
                     +strlen(tempCouleur)+nChiffre(getProfondeur())+43];

    sprintf(infos,"[%s : Position(%d,%d),Extremite(%d,%d),%s,Profondeur=%d]",tempId,getPosition().getX(),
            getPosition().getY(),getExtremite().getX(),getExtremite().getY(),tempCouleur,getProfondeur());

    return infos;
}
