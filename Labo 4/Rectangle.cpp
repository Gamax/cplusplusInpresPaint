#include <string.h>
#include <stdio.h>
#include "Rectangle.h"

Rectangle::Rectangle(void) : Forme() {
    setDimX(0);
    setDimY(0);
    setRempli(false);
}

Rectangle::Rectangle(char *id,const Point &pos,int dimx,int dimy,bool rempli,Couleur* couleur, int profondeur)
        : Forme(id,pos,couleur,profondeur){
    setDimX(dimx);
    setDimY(dimy);
    setRempli(rempli);
}

Rectangle::Rectangle(const Rectangle& rectangle) : Forme(rectangle){
    setDimX(rectangle.getDimX());
    setDimY(rectangle.getDimY());
    setRempli(rectangle.isRempli());
}

int Rectangle::getDimX() const {
    return dimX;
}

void Rectangle::setDimX(int dimX) {
    Rectangle::dimX = dimX;
}

int Rectangle::getDimY() const {
    return dimY;
}

void Rectangle::setDimY(int dimY) {
    Rectangle::dimY = dimY;
}

bool Rectangle::isRempli() const {
    return rempli;
}

void Rectangle::setRempli(bool rempli) {
    Rectangle::rempli = rempli;
}

ostream& operator<<(ostream& stream, const Rectangle& r){

    stream << "[";

    if(r.getId()!=NULL){
        stream << r.getId();
    }else{
        stream << "Aucun nom";
    }

    int temp = 0;

    if(r.isRempli())
        temp =1;

    stream << " : Position(" << r.getPosition().getX() << "," << r.getPosition().getY() << "),DimX="
           << r.getDimX() <<",DimY="<< r.getDimY()<<",Rempli="<< temp << ",";

    if(r.getCouleur()!= NULL){ //pas de couleur
        stream << "Couleur(" << r.getCouleur()->getRouge() << "," << r.getCouleur()->getVert() << ","
               << r.getCouleur()->getBleu()<<")";

    }else{
        stream << "Aucune couleur";
    }

    stream << ",Profondeur=" << r.getProfondeur()<<"]";

    return stream;
}

char* Rectangle::getInfos() {

    if(infos != NULL)
        delete(infos);

    //génération de la partie couleur de la string

    char *tempCouleur;

    if(getCouleur() !=NULL){

        tempCouleur = new char[22];

        sprintf(tempCouleur,"Couleur(%d,%d,%d)",getCouleur()->getRouge(),getCouleur()->getVert(),getCouleur()->getBleu());

    }else{ //pas de couleur

        tempCouleur = new char[15];

        strcpy(tempCouleur,"Aucune Couleur");
    }

    char* tempId;

    if(getId() != NULL){
        tempId = new char[strlen(getId())];

        strcpy(tempId,getId());
    }else{ //pas de nom de forme
        tempId = new char[10];

        strcpy(tempId,"Aucun nom");
    }

    int temp =0;

    if(isRempli())
        temp = 1;

    infos = new char[strlen(tempId)+nChiffre(getPosition().getX()) +nChiffre(getPosition().getY())
                     +nChiffre(getDimX())+nChiffre(getDimY())
                     +strlen(tempCouleur)+nChiffre(getProfondeur())+51];

    sprintf(infos,"[%s : Position(%d,%d),DimX=%d,DimY=%d,Rempli=%d,%s,Profondeur=%d]",tempId,getPosition().getX(),
            getPosition().getY(),getDimX(),getDimY(),temp,tempCouleur,getProfondeur());

    return infos;
}
