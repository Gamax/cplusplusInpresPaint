//
// Created by thomas on 19/01/18.
//


#ifndef CPLUSPLUSINPRESPAINT_SMARTPOINTER_H
#define CPLUSPLUSINPRESPAINT_SMARTPOINTER_H
#include <iostream>
#include "Ligne.h"
#include "Forme.h"


using namespace std;

template<class T>
class SmartPointer;

template <class T>
ostream& operator<<(ostream &s, const SmartPointer<T>& t);

template<class T>
class SmartPointer {

private:
    T * val;
public:
    SmartPointer();
    SmartPointer(T* t);
    //copie et = pointeront sur le même objet T !
    SmartPointer(const SmartPointer<T>& base );
    ~SmartPointer();
    void Delete();
    SmartPointer<T>& operator=(const SmartPointer<T>& base);
    SmartPointer<T>& operator=(T* base);
    T& operator*() const;
    T* operator->();
    bool operator<(const SmartPointer<T>& right) const;
    bool operator>(const SmartPointer<T>& right) const;
    bool operator==(const SmartPointer<T>& right) const;
    friend ostream& operator<< <>(ostream &s, const SmartPointer<T>& t);
    T* getVal();

};

template class SmartPointer<int>;
template class SmartPointer<int*>;
template class SmartPointer<Forme>;





#endif //CPLUSPLUSINPRESPAINT_SMARTPOINTER_H
