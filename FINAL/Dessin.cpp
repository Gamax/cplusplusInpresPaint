//
// Created by thomas on 20/01/18.
//

#include "Dessin.h"
#include "Iterateur.h"
#include <string.h>
#include "BaseException.h"
#include <fstream>
#include <cstdlib>
#include "Couleur.h"
#include "Forme.h"
#include "Rectangle.h"
#include "Ligne.h"
#include "Pixel.h"
#include "./WindowSDL/WindowSDL.h"

Dessin::Dessin() {

    setLargeur(0);
    setHauteur(0);
    setCouleur(Couleur(0,0,0));

}

Dessin::Dessin(int l, int h, const Couleur &f) {
    setLargeur(l);
    setHauteur(h);
    setCouleur(f);
}

void Dessin::ajouteCouleur(const Couleur &c) {
    Iterateur<Couleur> it(palette);

    while(!it.end()){
        if(strcmp(c.getNom(),((Couleur)it).getNom()) == 0)  //check si nom déjà utilisé
            throw BaseException("Nom couleur identique");
        it++;
    }

    palette.insere(c);
}

void Dessin::ajouteForme(Forme *pf, const char *nomCouleur) {
    //check si pas de forme avec même nom

    Iterateur<SmartPointer<Forme> > it(formes);


    while(!it.end()){

        SmartPointer<Forme> f = (SmartPointer<Forme>) it; //on obtiens le smart pointer

        if(strcmp((*f).getId(),pf->getId()) == 0) //nom égaux
            throw BaseException("Nom forme identique");

        it++;
    }


    //trouver couleur à associer

    Iterateur<Couleur> it2(palette);


    while(!it2.end()){
        if(strcmp(nomCouleur,((Couleur)it2).getNom()) == 0) {  //chercher couleur dans palette
            pf->setCouleur(&it2); //on set la couleur
            formes.insere(pf); //insertion possible car redef = T* dans smartpointer
            return;
        }
        it2++;
    }

    throw BaseException("Pas de couleur trouvée");


}



Dessin::~Dessin() {
    Iterateur<SmartPointer<Forme> > it(formes);

    while(!it.end()){

        it.remove().Delete();
    }

    Iterateur<Couleur> it2(palette);

    while(!it2.end()){
        it2.remove();
    }
}

void Dessin::setLargeur(const int l)
{
    largeur = l;
}

void Dessin::setHauteur(const int h)
{
    hauteur = h;
}

void Dessin::setCouleur(const Couleur& c)
{
    fond = c;
}

int Dessin::getLargeur() const
{
    return largeur;
}

int Dessin::getHauteur() const
{
    return hauteur;
}

const Couleur& Dessin::getCouleur() const {
    return fond;
}

void Dessin::modifierCouleur(const char *nomCouleur, int r, int v, int b) {
    Iterateur<Couleur> it(palette);

    while(!it.end()){
        if(strcmp(nomCouleur,((Couleur)it).getNom()) == 0) {  //couleur trouvée
            (&it)->setRouge(r);
            (&it)->setVert(v);
            (&it)->setBleu(b);
            return;

        }
        it++;
    }

    throw BaseException("Pas de couleur trouvée");
}

void Dessin::eclaicirCouleur(const char *nomCouleur, int l) {
    Iterateur<Couleur> it(palette);

    while(!it.end()){
        if(strcmp(nomCouleur,((Couleur)it).getNom()) == 0) {  //couleur trouvée
            *(&it) = ((Couleur)it)+l;
            return;

        }
        it++;
    }

    throw BaseException("Pas de couleur trouvée");
}

void Dessin::assombrirCouleur(const char *nomCouleur, int l) {
    Iterateur<Couleur> it(palette);

    while(!it.end()){
        if(strcmp(nomCouleur,((Couleur)it).getNom()) == 0) {  //couleur trouvée
            *(&it) = ((Couleur)it)-l;
            return;

        }
        it++;
    }

    throw BaseException("Pas de couleur trouvée");
}

void Dessin::supprimerCouleur(const char *nomCouleur) {


    Iterateur<SmartPointer<Forme> > it2(formes);
    Iterateur<Couleur> it(palette);

    while(!it.end()){
        if(strcmp(nomCouleur,((Couleur)it).getNom()) == 0) {  //couleur trouvée

            while(!it2.end()){

                if(strcmp(nomCouleur,((SmartPointer<Forme>) it2).getVal()->getCouleur()->getNom()) == 0 ){ //une forme utilise tjs la couleur
                    throw BaseException("Couleur toujours utilisée");

                }

                it2++;
            }

            it.remove();
            return;

        }
        it++;
    }

    throw BaseException("Pas de couleur trouvée");
}

void Dessin::supprimerForme(const char *nomForme) {
    Iterateur<SmartPointer<Forme> > it(formes);

    while(!it.end()){
        if(strcmp(nomForme,((SmartPointer<Forme>) it).getVal()->getId()) == 0){ //forme trouvée
            it.remove().Delete();
            return;
        }
        it++;
    }

    throw BaseException("Pas de forme trouvée");
}

void Dessin::modifierCouleurForme(const char* nomForme, const char* nomCouleur){
    Iterateur<SmartPointer<Forme> > it(formes);
    Iterateur<Couleur> it2(palette);

    while(!it.end()){
        if(strcmp(nomForme,((SmartPointer<Forme>) it).getVal()->getId()) == 0){ //forme trouvée

            while(!it2.end()){
                if(strcmp(nomCouleur,((Couleur)it2).getNom()) == 0){ //couleur trouvée
                    (((SmartPointer<Forme>) it).getVal())->setCouleur(&it2);
                    return;

                }
                it2++;
            }

            throw BaseException("Pas de couleur trouvée");
        }
        it++;
    }

    throw BaseException("Pas de forme trouvée");
}

void Dessin::AfficheCouleur() {
    Iterateur<Couleur> it(palette);

    while(!it.end()){
        ((Couleur)it ).Affiche();

        it++;
    }
}



void Dessin::AfficheForme() {
    Iterateur<SmartPointer<Forme> > it(formes);

    while(!it.end()){
        SmartPointer<Forme> forme = (SmartPointer<Forme>) it;

        cout << (*forme).getInfos() << endl;

        it++;
    }
}

void Dessin::AfficheUneForme(const char* nomForme) {
    Iterateur<SmartPointer<Forme> > it(formes);

    while(!it.end()){
        if(strcmp(nomForme,((SmartPointer<Forme>) it).getVal()->getId()) == 0){ //forme trouvée

            cout << (*((SmartPointer<Forme>) it)).getInfos() << endl;

            return;
        }
        it++;
    }

    throw BaseException("Pas de forme trouvée");
}

void Dessin::importCouleurs(const char *nomFichier) {
    ifstream fichier(nomFichier,ios::in);

    if(!fichier)
        throw BaseException("Erreur ouverture fichier Couleurs");

    char c;
    char buffer[500];
    int i = 0,numChamp = 0;
    Couleur temp;

    while (1) {

        c = (char) fichier.get();

        if(c == ';' || c == ':' || c == ',' || c == '\n' || c == EOF){ //on arrive à la fin d'une valeur

            buffer[i]='\0';

            //On gère la valeur

            try {


                switch (numChamp) {
                    case 0: //rouge
                        temp.setRouge(atoi(buffer));
                        break;
                    case 1: //vert
                        temp.setVert(atoi(buffer));
                        break;
                    case 2: //bleu
                        temp.setBleu(atoi(buffer));
                        break;
                    case 3: //nom
                        temp.setNom(buffer);
                        break;
                }



                numChamp++; //on avance de champ après avoir traité la valeur

                if(numChamp == 4) { // on revient au premier champ et on traite la couleur
                    numChamp = 0;
                    ajouteCouleur(temp);
                }


            }catch(BaseException e){
                cout << e.getMessage() << endl;
                numChamp = 0;

                while(c != '\n' && c != EOF) { //on fait un retour à la ligne
                    c = (char) fichier.get();
                    if (c == EOF || c == '\n')
                        break;
                }
            }

            i = 0; //on remet à 0 après avoir traité la valeur

            if(c == EOF) //on sort si on arrive à la fin du fichier
                break;

        }else{ //on lit tjs des caractères utiles
            buffer[i] = c;
            i++;
        }

    }

}

void Dessin::importRectangles(const char *nomFichier) {
    ifstream fichier(nomFichier,ios::in);

    if(!fichier)
        throw BaseException("Erreur ouverture fichier Rectangles");

    char c;
    char buffer[500];
    int i = 0,numChamp = 0;
    Rectangle *temp = new Rectangle;
    Point ptemp;
    int id = 1000;
    char nomForme[100];

    while (1) {

        c = (char) fichier.get();

        if(c == ';' || c == ':' || c == ',' || c == '\n' || c == EOF){ //on arrive à la fin d'une valeur

            buffer[i]='\0';

            //On gère la valeur

            try {


                switch (numChamp) {
                    case 0: //pos x
                        ptemp.setX(atoi(buffer));
                        temp->setPosition(ptemp);
                        break;
                    case 1: //pos y
                        ptemp = temp->getPosition();
                        ptemp.setY(atoi(buffer));
                        temp->setPosition(ptemp);
                        break;
                    case 2: //dim x
                        temp->setDimX(atoi(buffer));
                        break;
                    case 3: //dim y
                        temp->setDimY(atoi(buffer));
                        break;
                    case 4: //rempli
                        temp->setRempli(atoi(buffer));
                        break;
                    case 5: //profondeur
                        temp->setProfondeur(atoi(buffer));
                        break;
                    case 6: //couleur

                        sprintf(nomForme,"R%d",id);
                        id++;
                        temp->setId(nomForme);
                        ajouteForme(temp,buffer);
                        temp = new Rectangle;
                        break;
                }



                numChamp++; //on avance de champ après avoir traité la valeur

                if(numChamp == 7) { // on revient au premier champ et on traite la couleur
                    numChamp = 0;
                }


            }catch(BaseException e){
                cout << e.getMessage() << endl;
                numChamp = 0;

                while(c != '\n' && c != EOF) { //on fait un retour à la ligne
                    c = (char) fichier.get();
                    if (c == EOF || c == '\n')
                        break;
                }
            }

            i = 0; //on remet à 0 après avoir traité la valeur

            if(c == EOF) //on sort si on arrive à la fin du fichier
                break;

        }else{ //on lit tjs des caractères utiles
            buffer[i] = c;
            i++;
        }

    }

    fichier.close();
}

void Dessin::importLigne(const char *nomFichier) {
    ifstream fichier(nomFichier,ios::in);

    if(!fichier)
        throw BaseException("Erreur ouverture fichier Rectangles");

    char c;
    char buffer[500];
    int i = 0,numChamp = 0;
    Ligne *temp = new Ligne;
    Point ptemp;
    Point extemp;
    int id = 1000;
    char nomForme[100];

    while (1) {

        c = (char) fichier.get();

        if(c == ';' || c == ':' || c == ',' || c == '\n' || c == EOF){ //on arrive à la fin d'une valeur

            buffer[i]='\0';

            //On gère la valeur

            try {


                switch (numChamp) {
                    case 0: //pos x
                        ptemp.setX(atoi(buffer));
                        temp->setPosition(ptemp);
                        break;
                    case 1: //pos y
                        ptemp = temp->getPosition();
                        ptemp.setY(atoi(buffer));
                        temp->setPosition(ptemp);
                        break;
                    case 2: //ext x
                        extemp.setX(atoi(buffer));
                        temp->setExtremite(extemp);
                        break;
                    case 3: //ext y
                        extemp = temp->getExtremite();
                        extemp.setY(atoi(buffer));
                        temp->setExtremite(extemp);
                        break;
                    case 4: //profondeur
                        temp->setProfondeur(atoi(buffer));
                        break;
                    case 5: //couleur

                        sprintf(nomForme,"L%d",id);
                        id++;
                        temp->setId(nomForme);
                        ajouteForme(temp,buffer);
                        temp = new Ligne;
                        break;
                }



                numChamp++; //on avance de champ après avoir traité la valeur

                if(numChamp == 6) { // on revient au premier champ et on traite la couleur
                    numChamp = 0;
                }


            }catch(BaseException e){
                cout << e.getMessage() << endl;
                numChamp = 0;

                while(c != '\n' && c != EOF) { //on fait un retour à la ligne
                    c = (char) fichier.get();
                    if (c == EOF || c == '\n')
                        break;
                }
            }

            i = 0; //on remet à 0 après avoir traité la valeur

            if(c == EOF) //on sort si on arrive à la fin du fichier
                break;

        }else{ //on lit tjs des caractères utiles
            buffer[i] = c;
            i++;
        }

    }

    fichier.close();
}

void Dessin::importPixels(const char *nomFichier) {
    ifstream fichier(nomFichier,ios::in);

    if(!fichier)
        throw BaseException("Erreur ouverture fichier Rectangles");

    char c;
    char buffer[500];
    int i = 0,numChamp = 0;
    Pixel *temp = new Pixel;
    Point ptemp;
    int id = 1000;
    char nomForme[100];

    while (1) {

        c = (char) fichier.get();

        if(c == ';' || c == ':' || c == ',' || c == '\n' || c == EOF){ //on arrive à la fin d'une valeur

            buffer[i]='\0';

            //On gère la valeur

            try {


                switch (numChamp) {
                    case 0: //pos x
                        ptemp.setX(atoi(buffer));
                        temp->setPosition(ptemp);
                        break;
                    case 1: //pos y
                        ptemp = temp->getPosition();
                        ptemp.setY(atoi(buffer));
                        temp->setPosition(ptemp);
                        break;
                    case 2: //profondeur
                        temp->setProfondeur(atoi(buffer));
                        break;
                    case 3: //couleur

                        sprintf(nomForme,"P%d",id);
                        id++;
                        temp->setId(nomForme);
                        ajouteForme(temp,buffer);
                        temp = new Pixel;
                        break;
                }



                numChamp++; //on avance de champ après avoir traité la valeur

                if(numChamp == 4) { // on revient au premier champ et on traite la couleur
                    numChamp = 0;
                }


            }catch(BaseException e){
                cout << e.getMessage() << endl;
                numChamp = 0;

                while(c != '\n' && c != EOF) { //on fait un retour à la ligne
                    c = (char) fichier.get();
                    if (c == EOF || c == '\n')
                        break;
                }
            }

            i = 0; //on remet à 0 après avoir traité la valeur

            if(c == EOF) //on sort si on arrive à la fin du fichier
                break;

        }else{ //on lit tjs des caractères utiles
            buffer[i] = c;
            i++;
        }

    }

    fichier.close();

}

void Dessin::Save(const char* nomFichier)
{
    ofstream fichier(nomFichier, ios::out);
    if(!fichier)
    {
        throw BaseException("Erreur ouverture fichier Save Dessin");
    }

    int l = getLargeur();
    fichier.write((char*)&l, sizeof(int));

    int h = getHauteur();
    fichier.write((char*)&h, sizeof(int));

    fond.Save(fichier); //on sauve la couleur du fond

    cout.flush();

    //SAUVEGARDE PALETTE
    Iterateur<Couleur> it(palette);
    unsigned short nbrCouleur = 0;

    while(!it.end()){
        nbrCouleur++;
        it++;
    }
    cout.flush();

    fichier.write((char*)&nbrCouleur, sizeof(unsigned short)); //sauver le nbr de couleurs stockées

    it.reset();

    cout.flush();

    while(!it.end()){
        
        ((Couleur)it).Save(fichier);//on sauve la couleur
        it++;
    }

    //SAUVEGARDE LISTE FORME
    unsigned short nbrForme = 0;
    Iterateur<SmartPointer<Forme> > it2(formes);

    while(!it2.end()){
        nbrForme++;
        it2++;
    }

    fichier.write((char *)&nbrForme, sizeof(unsigned short));

    it2.reset();

    Rectangle *rectTemp;
    Pixel *pixTemp;
    Ligne *ligTemp;
    char temp;

    while(!it2.end()){
        SmartPointer<Forme> f = ((SmartPointer<Forme>) it2);

        rectTemp = dynamic_cast<Rectangle*>(f.getVal());
        if(rectTemp != NULL){ //c'est un rectangle
            temp = 'R';
            fichier.write((char*)&temp,sizeof(char));
            rectTemp->Save(fichier);
        }else {

            pixTemp = dynamic_cast<Pixel*>(f.getVal());
            if (pixTemp != NULL) { //c'est un pixel
                temp = 'P';
                fichier.write((char*)&temp,sizeof(char));
                pixTemp->Save(fichier);
            } else {

                ligTemp = dynamic_cast<Ligne*>(f.getVal());
                if (ligTemp != NULL) { //c'est une ligne
                    temp = 'L';
                    fichier.write((char*)&temp,sizeof(char));
                    ligTemp->Save(fichier);
                }
            }
        }
        it2++;
    }


    fichier.close();
}


void Dessin::Load(const char* nomFichier)
{
    ifstream fichier(nomFichier, ios::in);
    if(!fichier)
    {
        throw BaseException("Erreur d'ouverture fichier Dessin");
    }

    int l = 0;
    fichier.read((char*)&l, sizeof(int));
    setLargeur(l);

    int h = 0;
    fichier.read((char*)&h, sizeof(int));
    setHauteur(h);

    fond.Load(fichier); //on load le fond

    //LOAD PALETTE
    unsigned short nbrCouleur = 0;
    fichier.read((char*)&nbrCouleur, sizeof(unsigned short));

    for(int i = 0; i<nbrCouleur; i++)
    {
        Couleur tmpCouleur;
        tmpCouleur.Load(fichier);
        ajouteCouleur(tmpCouleur);
    }

    //LOAD LISTE FORME
    unsigned short nbrForme = 0;
    fichier.read((char*)&nbrForme, sizeof(unsigned short));

    char temp;

    Rectangle *rectTemp;
    Pixel *pixTemp;
    Ligne *ligTemp;
    Couleur *coulTemp;

    for(int i = 0; i<nbrForme; i++)
    {
        fichier.read(&temp,sizeof(char));

        switch (temp){
            case 'R':
                rectTemp = new Rectangle();
                rectTemp->Load(fichier);
                coulTemp = rectTemp->getCouleur();
                ajouteForme(rectTemp,coulTemp->getNom());
                delete coulTemp;
                break;
            case 'P':
                pixTemp = new Pixel();
                pixTemp->Load(fichier);
                coulTemp = pixTemp->getCouleur();
                ajouteForme(pixTemp,coulTemp->getNom());
                delete coulTemp;
                break;
            case 'L':
                ligTemp = new Ligne();
                ligTemp->Load(fichier);
                coulTemp = ligTemp->getCouleur();
                ajouteForme(ligTemp,coulTemp->getNom());
                delete coulTemp;
                break;
        }
    }

    fichier.close();
}

void Dessin::dessine() {



    WindowSDL::setBackground(getCouleur().getRouge(),getCouleur().getVert(),getCouleur().getBleu());

    Iterateur<SmartPointer<Forme> > it(formes);

    while(!it.end()){
        SmartPointer<Forme> forme = (SmartPointer<Forme>) it;

        (*forme).dessine();

        it++;
    }
}
