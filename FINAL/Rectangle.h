#ifndef CPLUSPLUSINPRESPAINT_RECTANGLE_H
#define CPLUSPLUSINPRESPAINT_RECTANGLE_H


#include "Forme.h"


class Rectangle: public Forme{

    friend ostream& operator<<(ostream& stream, const Rectangle& r);

private:
    int dimX;
    int dimY;
    bool rempli;

public:

    Rectangle(void);

    Rectangle(char *id,const Point &pos,int dimx,int dimy,bool rempli,Couleur* couleur = NULL, int profondeur  = 0);

    Rectangle(const Rectangle& rectangle);

    ~Rectangle();

    void setId(char *id);

    int getDimX() const;

    void setDimX(int dimX);

    int getDimY() const;

    void setDimY(int dimY);

    bool isRempli() const;

    void setRempli(bool rempli);

    char* getInfos();

    void Save(ofstream& fichier) const;
    void Load(ifstream& fichier);

    void dessine();

};


#endif //CPLUSPLUSINPRESPAINT_RECTANGLE_H
