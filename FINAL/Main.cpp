//
// Created by thomas on 21/01/18.
//

#include <stdlib.h>
#include <iostream>
#include "Dessin.h"
#include "BaseException.h"
#include "InvalidColorException.h"
#include "./WindowSDL/WindowSDL.h"
using namespace std;

#include "SmartPointer.h"
#include "Forme.h"
#include "Ligne.h"
#include "Pixel.h"
#include "Rectangle.h"
#include "ListeTriee.h"
#include "Iterateur.h"

void Affiche()
{
    cout << "********************** Inpres Paint **********************" << endl <<endl;
    cout << "\t0. Quitter" << endl;
    cout << "\t1. Nouveau Dessin" << endl;
    cout << "\t2. Ouvrir Dessin" << endl;
    cout << "\t3. Enregistrer Dessin" << endl;
    cout << "\t4. Fermer Dessin" << endl;
    cout << endl << "********************** Gestion generale **********************" << endl << endl;
    cout << "\t5. Afficher les parametres du dessin (taille et couleur de fond)" << endl;
    cout << "\t6. Specifier une couleur de fond" << endl;
    cout << "\t7. Afficher l'etat de la memoire" << endl;
    cout << endl << "********************** Gestion de la palette de couleurs ********************** "<< endl << endl;
    cout << "\t8. Ajouter une couleur" << endl;
    cout << "\t9. Modifier une couleur" << endl;
    cout << "\t10. Supprimer une couleur" << endl;
    cout << "\t11. Afficher toutes les couleurs de la palette" << endl;
    cout << "\t12. Importer une palette de couleurs a partir d'un fichier csv" << endl;
    cout << endl << "********************** Gestion des formes geometriques **********************" << endl << endl;
    cout << "\t13. Ajouter une forme" << endl;
    cout << "\t14. Supprimer une forme" << endl;
    cout << "\t15. Modifier la couleur d'une forme" << endl;
    cout << "\t16. Afficher les infos de toutes les formes" << endl;
    cout << "\t17. Afficher les infos sur une forme" << endl;
    cout << "\t18. Importer un ensemble de formes a partir d'un fichier csv" << endl;
}

int Choix()
{
    int tmp = 0;
    while(1)
    {
        Affiche();
        cout << "Choix : ";
        cin >> tmp;
        if (tmp < 0 || tmp > 18)
        {
            cout << "Saisie erronée" << endl << endl;
        }
        else
        {
            return tmp;
        }
    }
}

int main(){

    int choix = 1,choix2,largeurDessin,hauteurDessin;
    int r,v,b,profondeur,dimX,dimY,posX,posY,extX,extY;
    bool rempli;

    char Saisie[100],NomFichier[100],NomForme[100];

    Rectangle* rectTemp = NULL;
    Pixel* pixTemp = NULL;
    Ligne* ligTemp = NULL;

    Dessin* d = NULL;
    Couleur fond(255,255,255);

    while(choix != 0){

     choix = Choix();

    switch(choix) {
        case 0 :

            if(d!=NULL){
                delete(d);
                WindowSDL::close();
            }


            return 0;
        case 1: //nouv dessin

            if(d != NULL){
                cout << "Dessin déjà ouvert" << endl;
                break;
            }

            cout << "Largeur : ";
            cin >> largeurDessin;
            cout << "Hauteur : ";
            cin >> hauteurDessin;

            d = new Dessin(largeurDessin,hauteurDessin,Couleur(255,255,255));
            WindowSDL::open(largeurDessin,hauteurDessin);
            WindowSDL::setBackground(255,255,255);

            cout << "Dessin crée avec succès !" << endl;

            break;

        case 2://ouvrir dessin
            if(d != NULL){
                cout << "Dessin déjà ouvert" << endl;
                break;
            }

            cout << "Nom du fichier :";
            cin >> Saisie;

            sprintf(NomFichier,"%s.des",Saisie);
            try {
                d = new Dessin(750, 750, fond);
                d->Load(NomFichier);

                WindowSDL::open(d->getLargeur(),d->getHauteur());
                d->dessine();

                cout << "Dessin ouvert avec succès !" << endl;

            }catch (BaseException e){
                cout << e.getMessage() << endl;
                d = NULL;
                delete(d);
            }
            break;
        case 3:
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            cout << "Nom du fichier :";

            cin >> Saisie;

            sprintf(NomFichier,"%s.des",Saisie);

            d->Save(NomFichier);

            cout << "Dessin sauvé avec succès !" << endl;

            break;

        case 4: //fermer dessin
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            delete(d);
            d = NULL;

            WindowSDL::close();

            cout << "Dessin fermé" << endl;

            break;
        case 5://Afficher param dessin
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            cout << "------ Paramètres de Dessin ------" << endl;
            cout << "Largeur : " << d->getLargeur() << endl;
            cout << "Hauteur : " << d->getHauteur() << endl;
            cout << "Couleur de fond : " << d->getCouleur() << endl;

            break;
        case 6://Modifier couleur fond
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            try {

                cout << " Entrez nouvelle couleur de fond :" << endl;
                cout << "R :" << endl;
                cin >> r;
                cout << "V :" << endl;
                cin >> v;
                cout << "B :" << endl;
                cin >> b;

                d->setCouleur(Couleur(r, v, b));
                d->dessine();
                cout << "Couleur de fond changée avec succès !" << endl;

            }catch(InvalidColorException e){
                cout << e.getMessage() << endl;
            }

            break;
        case 7://afficher nombres objets couleurs
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            cout << "----- Etat mémoire -----" << endl;
            cout << "Nombre couleurs : " << Couleur::getCompteur() << endl;
            cout << "Nombre formes : " << Forme::getCompteur() << endl;
            cout << "Nombre points : " << Point::getCompteur() << endl;
            break;

        case 8://ajouter nouvelle couleur
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            cout << " Entrez nouvelle couleur :" << endl;
            cout << "R :" << endl;
            cin >> r;
            cout << "V :" << endl;
            cin >> v;
            cout << "B :" << endl;
            cin >> b;
            cout << "Nom :" << endl;
            cin >> Saisie;

            try{
                d->ajouteCouleur(Couleur(r,v,b,Saisie));
                cout << "Couleur ajoutée avec succès !" << endl;
            }catch(BaseException e){
                cout << e.getMessage() << endl;
            }



            break;

        case 9: //modif couleur palette
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "Quelle couleur voulez vous modifier ? : ";
            cin >> Saisie;
            cout << "De quelle façon ?" << endl << "1)Eclaircir" << endl << "2)Assombrir" << endl << "3)Modifier composantes" <<endl;
            cin >> choix2;

            if(choix2 > 3 || choix2 < 1){
                break;
            }

            try {


                switch (choix2) {
                    case 1:
                        cout << "Eclaircir de combien : ";
                        cin >> r;
                        d->eclaicirCouleur(Saisie, r);
                        break;
                    case 2:
                        cout << "Assombrir de combien : ";
                        cin >> r;
                        d->assombrirCouleur(Saisie, r);
                        break;
                    case 3:
                        cout << "Entrez les nouvelles composantes" << endl;
                        cout << "R :" << endl;
                        cin >> r;
                        cout << "V :" << endl;
                        cin >> v;
                        cout << "B :" << endl;
                        cin >> b;
                        d->modifierCouleur(Saisie, r, v, b);
                        break;
                }

                cout << "Couleur modifiée avec succès !" << endl;

                d->dessine();

            }catch(BaseException e){
                cout << e.getMessage() << endl;
            }

            break;
        case 10: //supprimer couleur
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "Quelle couleur voulez vous supprimer ? : ";
            cin >> Saisie;
            try {
                d->supprimerCouleur(Saisie);
                cout << "Couleur supprimée avec succès !" << endl;
            }catch(BaseException e){
                cout << e.getMessage() << endl;
            }



            break;
        case 11: //afficher liste couleur
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "---------- Liste des couleurs ----------" << endl;
            d->AfficheCouleur();
            break;

        case 12://importer csv couleur
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "Quel fichier voulez vous importer ? : ";
            cin >> Saisie;

            try {
                d->importCouleurs(Saisie);
                cout << "Fichier importé avec succès" << endl;
            }catch(BaseException e){
                cout << e.getMessage() << endl;
            }
            break;

        case 13: //ajouter forme
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            cout << "Quelle type de forme voulez vous ajouter ?" << endl;
            cout << "1) Rectangle" << endl;
            cout << "2) Ligne" << endl;
            cout << "3) Pixel" << endl;
            cin >> choix2;

            if(choix2 > 3 || choix2 < 1){
                break;
            }

            cout << "Nom :";
            cin >> NomForme;
            cout << "PosX :";
            cin >> posX;
            cout << "PosY :";
            cin >> posY;
            cout << "Profondeur : ";
            cin >> profondeur;
            cout << "Couleur :";
            cin >> Saisie;

            try {


                switch (choix2) {
                    case 1 : //rect
                        cout << "DimX :";
                        cin >> dimX;
                        cout << "DimY :";
                        cin >> dimY;
                        cout << "Rempli :";
                        cin >> rempli;

                        rectTemp = new Rectangle(NomForme, Point(posX, posY), dimX, dimY, rempli, NULL, profondeur);
                        d->ajouteForme(rectTemp, Saisie);
                        rectTemp = NULL;
                        break;
                    case 2: //ligne
                        cout << "ExtX :";
                        cin >> extX;
                        cout << "ExtY :";
                        cin >> extY;

                        ligTemp = new Ligne(NomForme,Point(posX,posY),Point(extX,extY),NULL,profondeur);
                        d->ajouteForme(ligTemp,Saisie);
                        ligTemp = NULL;
                        break;
                    case 3://Pixel
                        pixTemp = new Pixel(NomForme,Point(posX,posY),NULL,profondeur);
                        d->ajouteForme(pixTemp,Saisie);
                        pixTemp = NULL;

                }

                d->dessine();
                cout << "Forme ajoutée" << endl;

            }catch(BaseException e){
                cout << e.getMessage() << endl;
                if(ligTemp!=NULL)
                    delete(ligTemp);
                if(pixTemp!=NULL)
                    delete(pixTemp);
                if(rectTemp!=NULL)
                    delete(rectTemp);
            }
            break;

        case 14:
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "Quelle forme voulez vous supprimer ? : ";
            cin >> Saisie;
            try {
                d->supprimerForme(Saisie);
                d->dessine();
                cout << "Forme supprimée avec succès !" << endl;
            }catch(BaseException e){
                cout << e.getMessage() << endl;
            }

            break;

        case 15:
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "Quelle forme voulez vous modifier ? : ";
            cin >> NomForme;
            cout << "Nouvelle Couleur : ";
            cin >> Saisie;
            try {
                d->modifierCouleurForme(NomForme,Saisie);
                d->dessine();
                cout << "Forme modifiée avec succès !" << endl;
            }catch(BaseException e){
                cout << e.getMessage() << endl;
            }
            break;

        case 16:
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "---------- Liste des formes ----------" << endl;
            d->AfficheForme();
            break;

        case 17:
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }
            cout << "Id de la forme à afficher ? :";
            cin >> Saisie;

            try {
                d->AfficheUneForme(Saisie);
            }catch(BaseException e){
                cout << e.getMessage() << endl;
            }


            break;

        case 18:
            if(d == NULL){
                cout << "Pas de dessin ouvert" << endl;
                break;
            }

            cout << "Quel type de forme voulez vous importer ?" << endl;
            cout << "1) Rectangle" << endl;
            cout << "2) Ligne" << endl;
            cout << "3) Pixel" << endl;
            cin >> choix2;

            if(choix2 > 3 || choix2 < 1){
                break;
            }

            cout << "Nom du fichier : ";
            cin >> Saisie;

            try {

            switch (choix2){
                case 1:
                    d->importRectangles(Saisie);
                    break;
                case 2:
                    d->importLigne(Saisie);
                    break;
                case 3:
                    d->importPixels(Saisie);
                    break;
            }

            d->dessine();
                cout << "Fichier importé avec succès" << endl;

            }catch (BaseException e){
                cout << e.getMessage() << endl;
            }
            break;


    }

        cout << "Appuyez sur n'importe quelle touche pour revenir au menu..." << endl;
        system("read");
        system("clear");
        system("clear");

    }

    return 0;
}