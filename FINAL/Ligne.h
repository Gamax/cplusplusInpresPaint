#ifndef CPLUSPLUSINPRESPAINT_LIGNE_H
#define CPLUSPLUSINPRESPAINT_LIGNE_H

#include "Forme.h"

class Ligne: public Forme {

    friend ostream& operator<<(ostream& stream, const Ligne& l);

private:
    Point extremite;

public:
    Ligne(void);

    Ligne(char *id,const Point &pos,const Point &ext,Couleur* couleur = NULL, int profondeur  = 0);

    Ligne(const Ligne& Ligne);

    void setId(char *id);

    Point getExtremite() const;

    void setExtremite(const Point &p);

    char* getInfos();

    ~Ligne();
    
    //sauvegarde
    void Save(ofstream& fichier) const;
    void Load(ifstream& fichier);

    void dessine();
    
};


#endif //CPLUSPLUSINPRESPAINT_LIGNE_H
