//
// Created by thomas on 20/01/18.
//

#ifndef CPLUSPLUSINPRESPAINT_DESSIN_H
#define CPLUSPLUSINPRESPAINT_DESSIN_H

#include "Couleur.h"
#include "Liste.h"
#include "ListeTriee.h"


class Dessin
{
private :
    int largeur;
    int hauteur;
    Couleur fond;
    Liste<Couleur> palette;
    ListeTriee<SmartPointer<Forme> > formes;
public :
    Dessin() ;
    Dessin(int l,int h,const Couleur& f) ;
    void ajouteCouleur(const Couleur& c) ;
    void ajouteForme(Forme *pf,const char* nomCouleur) ;
    ~Dessin();

    //setters
    void setLargeur(const int l);
    void setHauteur(const int h);
    void setCouleur(const Couleur& c);

    //getters
    int getLargeur() const;
    int getHauteur() const;
    const Couleur& getCouleur() const;

    void modifierCouleur(const char* nomCouleur, int r, int v, int b);
    void eclaicirCouleur(const char* nomCouleur, int l);
    void assombrirCouleur(const char* nomCouleur, int l);

    void supprimerCouleur(const char* nomCouleur);
    void supprimerForme(const char* nomForme);

    void modifierCouleurForme(const char* nomForme, const char* nomCouleur);

    void AfficheCouleur();
    void AfficheForme();
    void AfficheUneForme(const char* nomForme);

    //import csv
    void importCouleurs(const char* nomFichier);
    void importRectangles(const char* nomFichier);
    void importLigne(const char* nomFichier);
    void importPixels(const char* nomFichier);

    //sauvegarde
    void Save(const char* nomFichier);
    void Load(const char* nomFichier);

    void dessine();
} ;


#endif //CPLUSPLUSINPRESPAINT_DESSIN_H
