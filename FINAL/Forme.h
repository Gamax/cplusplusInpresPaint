#ifndef CPLUSPLUSINPRESPAINT_FORME_H
#define CPLUSPLUSINPRESPAINT_FORME_H


#include "Point.h"
#include "Couleur.h"

class Forme {

protected:
    char* id = NULL;
    Point pos;
    Couleur* couleur = NULL;
    int profondeur;
    static int compteur;
    char* infos;

public:

    friend ostream& operator<<(ostream& stream, const Forme& f);

    //constructor

    Forme(void);

    Forme(const Point &pos,Couleur* couleur = NULL, int profondeur  = 0);

    Forme(const Forme& forme);

    //destructor

    ~Forme();

    //getter-setter



    char *getId() const;

    Point getPosition() const;

    Couleur *getCouleur() const;

    int getProfondeur() const;

    static int getCompteur();

    virtual char* getInfos()=0;

    virtual void setId(char *id)=0;

    void setPosition(const Point &pos);

    void setCouleur(Couleur *couleur);

    void setProfondeur(int profondeur);

    //files

    void Save(ofstream& fichier) const;

    void Load(ifstream& fichier);

    //other

    bool operator< (const Forme& right) const;

    bool operator> (const Forme& right) const;

    bool operator== (const Forme& right) const;

    void Affiche() const;

    virtual void dessine()=0;

protected:
    int nChiffre(int i);

};


#endif //CPLUSPLUSINPRESPAINT_FORME_H
