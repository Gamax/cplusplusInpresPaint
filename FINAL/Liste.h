//
// Created by thomas on 21/11/17.
//

#ifndef CPLUSPLUSINPRESPAINT_LISTE_H
#define CPLUSPLUSINPRESPAINT_LISTE_H

#include "ListeBase.h"


template<class T> class Liste : public ListeBase<T>{
public:
    T* insere(const T & val);
};

template class Liste<int>;
template class Liste<Couleur>;

#endif //CPLUSPLUSINPRESPAINT_LISTE_H
