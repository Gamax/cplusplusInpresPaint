#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "Pixel.h"
#include "BaseException.h"
#include <fstream>
#include "./WindowSDL/WindowSDL.h"



Pixel::Pixel(void) : Forme(){}

Pixel::Pixel(char *id,const Point &pos,Couleur* couleur, int profondeur)
        : Forme(pos,couleur,profondeur){
    setId(id);
}

Pixel::Pixel(const Pixel& pixel) : Forme(pixel){
    setId(pixel.getId());
}

Pixel::~Pixel() {

}

void Pixel::setId(char *id) {
    if(this->id != NULL)
        delete(this->id);

    if(id == NULL){
        throw BaseException("Id invalide !");
    }else{

        //vérif du format de id

        if(strlen(id)<2 || id[0] != 'P') //on vérifie que id>2 et que le 1ère lettre est bonne
            throw BaseException("Id invalide !");

        for(unsigned i=1;i<strlen(id);i++) //on vérifie que chaque caractère restant est bien un chiffre
            if(!isdigit(id[i]))
                throw BaseException("Id invalide !");


        this->id = new char[strlen(id)+1];
        strcpy(this->id,id);
    }
}

ostream& operator<<(ostream& stream, const Pixel& p){

    stream << "[";

    if(p.getId()!=NULL){
        stream << p.getId();
    }else{
        stream << "Aucun nom";
    }

    stream << " : Position(" << p.getPosition().getX() << "," << p.getPosition().getY() << "),";

    if(p.getCouleur()!= NULL){ //pas de couleur
        stream << "Couleur(" << p.getCouleur()->getRouge() << "," << p.getCouleur()->getVert() << ","
               << p.getCouleur()->getBleu()<<",";

        if(p.getCouleur()->getNom()!=NULL){ //couleur pas de nom
            stream << p.getCouleur()->getNom();
        }else{
            stream << "Aucun nom";
        }

        stream << ")";

    }else{
        stream << "Aucune couleur";
    }



    stream << ",Profondeur=" << p.getProfondeur()<<"]";

    return stream;
}

char* Pixel::getInfos() {

    if(infos != NULL)
        delete(infos);

    //génération de la partie couleur de la string

    char *tempCouleur;
    char *tempNomCouleur;

    if(getCouleur() !=NULL){


        if(getCouleur()->getNom()!=NULL){

            tempNomCouleur = new char[strlen(getCouleur()->getNom())];

            strcpy(tempNomCouleur,getCouleur()->getNom());

        }else{ //couleur pas de nom

            tempNomCouleur = new char[10];

            strcpy(tempNomCouleur,"Aucun nom");
        }

        tempCouleur = new char[strlen(tempNomCouleur)+22];

        sprintf(tempCouleur,"Couleur(%d,%d,%d,%s)",getCouleur()->getRouge(),getCouleur()->getVert(),getCouleur()->getBleu(),
                tempNomCouleur);

        delete(tempNomCouleur);

    }else{ //pas de couleur

        tempCouleur = new char[15];

        strcpy(tempCouleur,"Aucune Couleur");
    }

    char* tempId;

    if(getId() != NULL){
        tempId = new char[strlen(getId())];

        strcpy(tempId,getId());
    }else{ //pas de nom de forme
        tempId = new char[10];

        strcpy(tempId,"Aucun nom");
    }



    infos = new char[strlen(tempId)+nChiffre(getPosition().getX())
                     +nChiffre(getPosition().getY())+strlen(tempCouleur)+nChiffre(getProfondeur())+30];

    sprintf(infos,"[%s : Position(%d,%d),%s,Profondeur=%d]",tempId,getPosition().getX(),
            getPosition().getY(),tempCouleur,getProfondeur());

    return infos;
}

void Pixel::Save(ofstream& fichier) const
{

    if(getId()!=NULL)
    {

        if(!fichier)//s'il ne s'est pas bien ouvert
        {
            throw BaseException("Erreur d'ouverture fichier Save Pixel");
        }

        Forme::Save(fichier);

    }
}

void Pixel::Load(ifstream& fichier)
{

    if(!fichier)
    {
        throw BaseException("Erreur ouverture fichier Load Pixel");
    }
    Forme::Load(fichier);

}

void Pixel::dessine(){
    WindowSDL::setPixel(getCouleur()->getRouge(),getCouleur()->getVert(),getCouleur()->getBleu(),getPosition().getX(),getPosition().getY());
}
