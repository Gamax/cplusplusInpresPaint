#ifndef CPLUSPLUSINPRESPAINT_FORME_H
#define CPLUSPLUSINPRESPAINT_FORME_H

#include "Point.h"
#include "Couleur.h"

class Forme {

private:
    char* id = NULL;
    Point pos;
    Couleur* couleur = NULL;
    int profondeur;
    static int compteur;

public:

    //constructor

    Forme(void);

    Forme(char *id,const Point &pos,Couleur* couleur = NULL, int profondeur  = 0);

    Forme(const Forme& forme);

    //destructor

    ~Forme();

    //getter-setter



    char *getId() const;

    Point getPosition() const;

    Couleur *getCouleur() const;

    int getProfondeur() const;

    static int getCompteur();

    void setId(char *id);

    void setPosition(const Point &pos);

    void setCouleur(Couleur *couleur);

    void setProfondeur(int profondeur);

    //other

    void Affiche() const;

};


#endif //CPLUSPLUSINPRESPAINT_FORME_H
