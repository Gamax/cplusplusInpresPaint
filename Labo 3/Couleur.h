
#ifndef COULEUR_H
#define COULEUR_H

#include <iostream>

using namespace std;

class Couleur
{

    friend Couleur operator+ (int l,Couleur c);
    friend ostream& operator<<(ostream& stream, const Couleur& c);
    friend istream& operator>>(istream& stream, Couleur& c);

	private:
	int rouge;
	int vert;
	int bleu;
	char *nom;
	static int compteur;


	public:
    static Couleur ROUGE;
    static Couleur VERT;
    static Couleur BLEU;



	//constructeur par defaut
	Couleur(void);


	//constructeur
	Couleur(int r,int v,int b,const char *n);
	
	Couleur(int r, int v, int b);
	
	Couleur(const Couleur& Coul);

	//destructeur
	~Couleur(void);

    //operator
    Couleur& operator= (Couleur c);

    Couleur operator+ (Couleur c);

    Couleur operator+ (int l);

    Couleur operator- (int l);

    bool operator< (Couleur c);

    bool operator== (Couleur c);

    bool operator> (Couleur c);

    Couleur operator++();

    Couleur operator++(int);

    Couleur operator--();

    Couleur operator--(int);

	//setters
	void setRouge(int r);

	void setVert(int v);

	void setBleu(int b);

	void setNom(const char*n);


	//getters
	static int getCompteur();

	int getRouge() const;

	int getVert() const;

	int getBleu() const;

	const char * getNom() const;

	int getLuminance() const;

	//autre methode
	void Affiche(void) const;
};


#endif
