#ifndef CPLUSPLUSINPRESPAINT_LISTETRIEE_H
#define CPLUSPLUSINPRESPAINT_LISTETRIEE_H

#include "ListeBase.h"

template<class T> class ListeTriee : public ListeBase<T>{
public:
    T* insere(const T & val);
};

template class ListeTriee<int>;
template class ListeTriee<Couleur>;


#endif //CPLUSPLUSINPRESPAINT_LISTETRIEE_H
