#include <cstdio>
#include "Liste.h"
using namespace std;

template<class T>
T *Liste<T>::insere(const T &val) {
    Cellule<T> *temp;

    if(ListeBase<T>::pTete == NULL){ //cas où la liste est vide
        ListeBase<T>::pTete = new Cellule<T>;
        ListeBase<T>::pTete->valeur = val;
        ListeBase<T>::pTete->suivant = NULL;
        return &ListeBase<T>::pTete->valeur;
    }

    temp = ListeBase<T>::pTete;

    while(temp->suivant != NULL){
        temp = temp->suivant;
    }

    temp->suivant = new Cellule<T>;
    temp=temp->suivant;
    temp->valeur = val;
    temp->suivant = NULL;

    return &temp->valeur;
}