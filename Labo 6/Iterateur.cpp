//
// Created by thomas on 21/11/17.
//

#include "Iterateur.h"

//Constructeurs

template<class T>
Iterateur<T>::Iterateur(ListeBase<T> &l): liste(l) {
    currentCell = liste.pTete;
    previousCell = currentCell;
}

//Autres fonctions

template<class T>
void Iterateur<T>::reset() {
    currentCell = liste.pTete;
    previousCell = currentCell;
}

template<class T>
bool Iterateur<T>::end() {
    if(currentCell == NULL)
        return true;
    return false;
}

template<class T>
void Iterateur<T>::operator++(int) {
    if(currentCell != NULL) {
        previousCell = currentCell;
        currentCell = currentCell->suivant;
    }
}

template<class T>
Iterateur<T>::operator T(){
    return currentCell->valeur;
}

template<class T>
T *Iterateur<T>::operator&(){
    return &currentCell->valeur;
}

template<class T>
T Iterateur<T>::remove() {

    Cellule<T> *temp;
    T tempObjet;

    if(previousCell == currentCell){ //on est au début
        temp = currentCell;
        currentCell=currentCell->suivant;
        liste.pTete=currentCell;
        previousCell=currentCell;
        tempObjet = temp->valeur; //on fait une copie dynamique de l'objet pr la retourner
        delete (temp);
        return tempObjet;
    }

    tempObjet = currentCell->valeur;
    previousCell->suivant = currentCell->suivant; //on rattache la chaîne en enlevant currentCell
    delete (currentCell);

    currentCell=previousCell->suivant; //on met currentCell sur la cell suivant celle enlevée

    return tempObjet;
}
