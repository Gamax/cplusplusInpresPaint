#include "InvalidColorException.h"


InvalidColorException::InvalidColorException(char *msg, bool r, bool v, bool b) : BaseException(msg) {

    this->r=r;
    this->v=v;
    this->b=b;

}

bool InvalidColorException::rougeValide() const {
    return r;
}

bool InvalidColorException::vertValide() const {
    return v;
}

bool InvalidColorException::bleuValide() const {
    return b;
}

