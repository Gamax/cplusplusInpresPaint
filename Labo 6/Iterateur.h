//
// Created by thomas on 21/11/17.
//

#ifndef CPLUSPLUSINPRESPAINT_ITERATEUR_H
#define CPLUSPLUSINPRESPAINT_ITERATEUR_H


#include "ListeBase.h"

//template<class T> class ListeBase;

template <class T> class Iterateur {

private:
    ListeBase<T> &liste;
    Cellule<T> * currentCell;
    Cellule<T> * previousCell;

public:
    Iterateur(ListeBase<T> &l);
    void reset();
    bool end();
    void operator++(int);
    operator T();
    T* operator&();
    T remove();
};

template class Iterateur<int>;
template class Iterateur<Couleur>;


#endif //CPLUSPLUSINPRESPAINT_ITERATEUR_H
