#ifndef CPLUSPLUSINPRESPAINT_FORME_H
#define CPLUSPLUSINPRESPAINT_FORME_H


#include "Point.h"
#include "Couleur.h"

class Forme {

protected:
    char* id = NULL;
    Point pos;
    Couleur* couleur = NULL;
    int profondeur;
    static int compteur;
    char* infos;

public:

    //constructor

    Forme(void);

    Forme(const Point &pos,Couleur* couleur = NULL, int profondeur  = 0);

    Forme(const Forme& forme);

    //destructor

    ~Forme();

    //getter-setter



    char *getId() const;

    Point getPosition() const;

    Couleur *getCouleur() const;

    int getProfondeur() const;

    static int getCompteur();

    virtual char* getInfos()=0;

    virtual void setId(char *id)=0;

    void setPosition(const Point &pos);

    void setCouleur(Couleur *couleur);

    void setProfondeur(int profondeur);

    //other

    void Affiche() const;

protected:
    int nChiffre(int i);

};


#endif //CPLUSPLUSINPRESPAINT_FORME_H
