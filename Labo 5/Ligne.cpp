#include <string.h>
#include <stdio.h>
#include "Ligne.h"
#include "BaseException.h"

Ligne::Ligne(void) : Forme(){
    extremite.setX(0);
    extremite.setY(0);
}

Ligne::Ligne(char *id,const Point &pos,const Point &ext,Couleur* couleur, int profondeur)
        : Forme(pos,couleur,profondeur){
    setId(id);
    setExtremite(ext);
}

Ligne::Ligne(const Ligne& ligne) : Forme(ligne){
    setId(ligne.getId());
    setExtremite(ligne.getExtremite());
}

void Ligne::setId(char *id) {
    if(this->id != NULL)
        delete(this->id);

    if(id == NULL){
        throw BaseException("Id invalide !");
    }else{

        //vérif du format de id

        if(strlen(id)<2 || id[0] != 'L') //on vérifie que id>2 et que le 1ère lettre est bonne
            throw BaseException("Id invalide !");

        for(unsigned i=1;i<strlen(id);i++) //on vérifie que chaque caractère restant est bien un chiffre
            if(!isdigit(id[i]))
                throw BaseException("Id invalide !");


        this->id = new char[strlen(id)+1];
        strcpy(this->id,id);
    }
}

Point Ligne::getExtremite() const{
    return extremite;
}

void Ligne::setExtremite(const Point &p){
    Ligne::extremite = p;
}

ostream& operator<<(ostream& stream, const Ligne& l){

    stream << "[";

    if(l.getId()!=NULL){
        stream << l.getId();
    }else{
        stream << "Aucun nom";
    }

    stream << " : Position(" << l.getPosition().getX() << "," << l.getPosition().getY() << "),Extremite("
           << l.getExtremite().getX() <<","<< l.getExtremite().getY()<<"),";

    if(l.getCouleur()!= NULL){ //pas de couleur
        stream << "Couleur(" << l.getCouleur()->getRouge() << "," << l.getCouleur()->getVert() << ","
               << l.getCouleur()->getBleu()<<")";

    }else{
        stream << "Aucune couleur";
    }

    stream << ",Profondeur=" << l.getProfondeur()<<"]";

    return stream;
}

char* Ligne::getInfos() {

    if(infos != NULL)
        delete(infos);

    //génération de la partie couleur de la string

    char *tempCouleur;

    if(getCouleur() !=NULL){

        tempCouleur = new char[22];

        sprintf(tempCouleur,"Couleur(%d,%d,%d)",getCouleur()->getRouge(),getCouleur()->getVert(),getCouleur()->getBleu());

    }else{ //pas de couleur

        tempCouleur = new char[15];

        strcpy(tempCouleur,"Aucune Couleur");
    }

    char* tempId;

    if(getId() != NULL){
        tempId = new char[strlen(getId())];

        strcpy(tempId,getId());
    }else{ //pas de nom de forme
        tempId = new char[10];

        strcpy(tempId,"Aucun nom");
    }

    infos = new char[strlen(tempId)+nChiffre(getPosition().getX()) +nChiffre(getPosition().getY())
                     +nChiffre(getExtremite().getX())+nChiffre(getExtremite().getY())
                     +strlen(tempCouleur)+nChiffre(getProfondeur())+43];

    sprintf(infos,"[%s : Position(%d,%d),Extremite(%d,%d),%s,Profondeur=%d]",tempId,getPosition().getX(),
            getPosition().getY(),getExtremite().getX(),getExtremite().getY(),tempCouleur,getProfondeur());

    return infos;
}
