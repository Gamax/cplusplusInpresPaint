#ifndef CPLUSPLUSINPRESPAINT_PIXEL_H
#define CPLUSPLUSINPRESPAINT_PIXEL_H

#include "Forme.h"


class Pixel: public Forme{

    friend ostream& operator<<(ostream& stream, const Pixel& p);

public:
    Pixel(void);

    Pixel(char *id,const Point &pos,Couleur* couleur = NULL, int profondeur  = 0);

    Pixel(const Pixel& pixel);

    void setId(char *id);

    char* getInfos();

};


#endif //CPLUSPLUSINPRESPAINT_PIXEL_H
