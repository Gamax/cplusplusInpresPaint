
#ifndef COULEUR_H
#define COULEUR_H

class Couleur
{
	private:
	int rouge;
	int vert;
	int bleu;
	char *nom;


	public:
	//constructeur par defaut
	Couleur(void);


	//constructeur
	Couleur(int r,int v,int b,const char *n);
	
	Couleur(int r, int v, int b);
	
	Couleur(const Couleur& Coul);

	//destructeur
	~Couleur(void);


	//setters
	void setRouge(int r);

	void setVert(int v);

	void setBleu(int b);

	void setNom(const char*n);


	//getters
	int getRouge() const;

	int getVert() const;

	int getBleu() const;

	const char * getNom() const;

	//autre methode
	void Affiche(void) const;
};

#endif
