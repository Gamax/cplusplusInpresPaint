#include <iostream>
#include <string.h>
#include "Couleur.h"

using namespace std;

Couleur::Couleur(void)
{
	cout<<"Constructeur par defaut !"<<endl;
	setRouge(0);
	setVert(0);
	setBleu(0);
	nom=NULL;
	setNom("Noir");
}


//constructeur
Couleur::Couleur(int r,int v,int b,const char *n)
{
	cout<<"Constructeur 2"<<endl;
	setRouge(r);
	setVert(v);
	setBleu(b);
	nom=NULL;
	setNom(n);
}

Couleur::Couleur(int r, int v, int b){
	cout<<"Constructeur 3"<<endl;
	setRouge(r);
	setVert(v);
	setBleu(b);
	nom=NULL;
	
}


//copie
Couleur::Couleur(const Couleur& c){
	cout<<"Constructeur de copie"<<endl;
	setRouge(c.getRouge());
	setVert(c.getVert());
	setBleu(c.getBleu());
	nom=NULL;
	setNom(c.getNom());
}


//destructeur
Couleur::~Couleur(void)
{
	cout<<"Destructeur !"<<endl;
	if(nom) delete nom;
}


//setters
void Couleur::setRouge(int r)
{
	if(r>=0&&r<=255)
		rouge=r;
}

void Couleur::setVert(int v)
{
	if(v>=0&&v<=255)
		vert=v;
}

void Couleur::setBleu(int b)
{
	if(b>=0&&b<=255)
		bleu=b;
}

void Couleur::setNom(const char*n)
{
	if(nom) delete nom;
	if(!n)
		nom=NULL;
	else
	{
		nom=new char[strlen(n)+1];
		strcpy(nom,n);
	}
}


//getters
int Couleur::getRouge() const{ return rouge;}

int Couleur::getVert() const{ return vert;}

int Couleur::getBleu() const{ return bleu;}

const char * Couleur::getNom() const
{
	if(nom)
		return nom;
	return NULL;
}

//autre methode
void Couleur::Affiche(void) const
{


	cout<<"Rouge="<<rouge<<endl<<"Vert="<<vert<<endl<<"Bleu="<<bleu<<endl;
	
	if(nom){
		cout<<"Nom="<<nom<<endl;
	}
}
